import pic_2 from "../images/top-2.PNG";
import pic_3 from "../images/top-3.PNG";
import pic_4 from "../images/top-4.PNG";
import pic_5 from "../images/top-5.PNG";
import pic_6 from "../images/top-6.PNG";

function TotalJobs() {
    const tableTotal = () => {
        return (
            <div className="div-top">
                <table>
                    <tr>
                        <td className="td-top">
                            TOTAL <span style={{ color: "#ffa800" }}>14,138</span> JOBS IN <span style={{ color: "#ffa800" }}>MALAYSIA</span><br /><br />
                            <button className="btn-free"><b>FREE</b> SIGN UP</button>&nbsp;&nbsp;&nbsp;
                            <button className="btn-search">SEARCH JOBS</button><br /><br />
                            <div className="div-img-top">
                                <figure style={{ border: "white" }} class="figure">
                                    <img className="img-top" src={pic_2} height="51%" width="51%" />
                                    <figcaption className="caption">
                                        <b>TRENDING ARTICLES</b><br /><br />
                                        4 Principles on How to Win Friends & Influence People at Your Workplace<br /><br />
                                        <b><a href="">READ MORE</a></b>
                                    </figcaption>
                                </figure>
                            </div>
                        </td>
                        <td className="td-top">
                            <img src={pic_3} height="300px" width="300px" /><br /><br />
                            <div className="div-featured">
                                FEATURED EMPLOYERS<br /><br />
                                <a href=""><img className="img-mid" src={pic_4} height="50px" width="50px" /></a>
                                <a href=""><img className="img-left" src={pic_5} height="50px" width="50px" /></a>
                                <a href=""><img className="img-left" src={pic_6} height="50px" width="50px" /></a><br /><br />
                                <a href="">MORE</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }

    return (
        <div className="body">
            {tableTotal()}
        </div>
    )
}

export default TotalJobs;