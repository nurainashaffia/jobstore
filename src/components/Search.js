function Search() { 
    return (
        <div >
            <div className="body-2">
                <text className="text-search">IN JUST ONE MINUTE</text><br />
                SEARCH BY POPULAR CATEGORIES<br />
                <input type="text" className="form-control search" placeholder="Search by keyword or job title" />
                <div className="body-5">Popular categories
                    <a href="">Sales and Marketing jobs</a>
                    <a href="">Information Technology jobs</a>
                    <a href="">Accounting/Auditing jobs</a>
                    <a href="">Engineering jobs</a>
                    <a href="">Administrative jobs</a>
                    <a href="">Customer Service jobs</a>
                    <a href="">Human Resources jobs</a>
                    <a href="">Finance/Banking jobs</a>
                    <a href="">More</a><br /><br />
                </div>
            </div>
        </div>
    )
}

export default Search;