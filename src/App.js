import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from './components/Home';
import Menu from './components/Menu';

function App() {
  return (
    <Router>
      <Menu />
      <Routes>
        <Route path="/" element={<Home/>}></Route>
        {/* <Route path="/search-jobs" element={<Search/>}></Route>
        <Route path="/companies" element={<Companies/>}></Route> */}
      </Routes>
    </Router>
  );
}

export default App;
