import { Link } from "react-router-dom";

function Menu() {
    return (
        <div className="Navbar">
            <div>
                <Link to="/">
                    <text className="jobstore" >Jobstore.com</text><text className="registered">&reg;</text>
                </Link>
            </div>
            <div className="links-left">
                <Link to="/search-jobs">SEARCH JOBS</Link>
                <Link to="/companies">COMPANIES</Link>
                <Link to="/download">DOWNLOAD APP</Link>
                <Link to="/countries">COUNTRIES</Link>
            </div>
            <div className="links-right">
                <Link to="/sign-in">SIGN IN</Link>
                <Link to="/sign-up">SIGN UP</Link>
            </div>
            <div className="links-right-button">
                <button className="btn-employers">EMPLOYERS</button>
            </div>
        </div>
    );
}

export default Menu;
