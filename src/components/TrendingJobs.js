function TrendingJobs() {
    const tableTrending = () => {
        return (
            <div className="body-4">
                <table className="tb-trending">
                    <tr>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Key Account Executive<br />
                                    <text style={{ color: "black" }}>Klang</text>
                                </div>
                            </a>
                        </td>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Costing Executive (URGENT)<br />
                                    <text style={{ color: "black" }}>Klang</text>
                                </div>
                            </a>
                        </td>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Architectural Draughtsman<br />
                                    <text style={{ color: "black" }}>Johor Bahru</text>
                                </div>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Firmware Engineer<br />
                                    <text style={{ color: "black" }}>Senai</text>
                                </div>
                            </a>
                        </td>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Site Supervisor<br />
                                    <text style={{ color: "black" }}>Johor Bahru</text>
                                </div>
                            </a>
                        </td>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Field Service Support Engineer<br />
                                    <text style={{ color: "black" }}>Selangor / Melaka / Penang</text>
                                </div>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Merchandiser / Assistant Merchandiser<br />
                                    <text style={{ color: "black" }}>Johor Bahru</text>
                                </div>
                            </a>
                        </td>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Constracts Executive cum Quality<br />
                                    <text style={{ color: "black" }}>Kuala Lumpur</text>
                                </div>
                            </a>
                        </td>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Front Office Assistant<br />
                                    <text style={{ color: "black" }}>Bukit Damansara</text>
                                </div>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Project Engineer<br />
                                    <text style={{ color: "black" }}>Alor Gajah</text>
                                </div>
                            </a>
                        </td>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Executive - IT<br />
                                    <text style={{ color: "black" }}>Subang Jaya</text>
                                </div>
                            </a>
                        </td>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Account Assistant cum Administrator<br />
                                    <text style={{ color: "black" }}>Johor Bahru</text>
                                </div>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    M&E Assistant Resident Engineering<br />
                                    <text style={{ color: "black" }}>Kempas Baru</text>
                                </div>
                            </a>
                        </td>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Account Assistant<br />
                                    <text style={{ color: "black" }}>Kuala Lumpur</text>
                                </div>
                            </a>
                        </td>
                        <td className="td-trending">
                            <a href="">
                                <div>
                                    Product Supervisor Cum Technician<br />
                                    <text style={{ color: "black" }}>Kota Damansara</text>
                                </div>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }

    return (
        <div>
            <div className="body-2">
                TRENDING JOBS IN MALAYSIA<br />
                <a href="">view more</a>
            </div>
            {tableTrending()}
        </div>
    )
}

export default TrendingJobs;