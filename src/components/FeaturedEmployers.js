import pic_4 from "../images/top-4.PNG";
import pic_5 from "../images/top-5.PNG";
import pic_6 from "../images/top-6.PNG";
import pic_7 from "../images/mid-1.PNG";
import pic_8 from "../images/mid-2.PNG";
import pic_9 from "../images/mid-3.PNG";
import pic_10 from "../images/mid-4.PNG";

function FeaturedEmployers() {
    return (
        <div>
            <div className="body-2">
                <div className="ads"><a href=""><img src={pic_7} height="100%" width="100%" /></a></div>
                FEATURED EMPLOYERS<br />
                <a href="">view more</a>
            </div>
            <div className="body-3">
                <table className="tb-featured">
                    <tr>
                        <td className="td-featured">
                            <figure style={{ border: "white" }} class="figure">
                                <img className="img-mid" src={pic_8} height="51%" width="51%" />
                                <figcaption className="caption-featured">
                                    <b style={{ color: "black" }}>Tao Bin Sdn Bhd</b><br /><br />
                                    49-3 Jalan PJU 1/37 , Dataran Prima, 47301 Petaling Jaya, Selangor<br />
                                </figcaption>
                                <figcaption className="caption-btn">
                                    <img src={pic_4} height="35px" width="35px" />
                                    <button className="btn-apply">Apply Jobs</button>
                                </figcaption>
                            </figure>
                        </td>
                        <td className="td-featured">
                            <figure style={{ border: "white" }} class="figure">
                                <img className="img-mid" src={pic_9} height="51%" width="51%" />
                                <figcaption className="caption-featured">
                                    <b style={{ color: "black" }}>Wallpaper & Carpet Distributors</b><br /><br />
                                    Lot 1509, Batu 8, Jalan Klang Lama, Taman Desaria<br />
                                </figcaption>
                                <figcaption className="caption-btn">
                                    <img src={pic_5} height="35px" width="35px" />
                                    <button className="btn-apply">Apply Jobs</button>
                                </figcaption>
                            </figure>
                        </td>
                        <td className="td-featured">
                            <figure style={{ border: "white" }} class="figure">
                                <img className="img-mid" src={pic_10} height="51%" width="51%" />
                                <figcaption className="caption-featured">
                                    <b style={{ color: "black" }}>UST Global (M) Sdn Bhd</b><br /><br />
                                    Level 2, Mini Circuit Building 3, Plot 10, Bayan Lepas Technoplex, Phase IV, Bayan Lepas Industrial Zone, 11900 Bayan Lepas, Pulau Pinang<br />
                                </figcaption>
                                <figcaption className="caption-btn"><img src={pic_6} height="35px" width="35px" />
                                    <button className="btn-apply">Apply Jobs</button>
                                </figcaption>
                            </figure>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    )
}

export default FeaturedEmployers;