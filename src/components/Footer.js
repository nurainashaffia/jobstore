import { FaFacebookF, FaTwitter, FaLinkedinIn, FaInstagram } from "react-icons/fa";

function Footer() {
    const tableFooter = () => {
        return (
            <div>
                <table className="tb-footer">
                    <tr>
                        <td className="td-footer">
                            COMPANY<br /><br />
                            <a href="">About Us</a><br />
                            <a href="">Contact Support</a><br />
                            <a href="">Careers @ Jobstore</a><br />
                            <a href="">Blog</a><br />
                            <a href="">Partner Sites</a><br />
                            <a href="">Fulfillment Policy</a><br />
                        </td>
                        <td className="td-footer">
                            JOBSEEKER<br /><br />
                            <a href="">Sign Up</a><br />
                            <a href="">Sign In</a><br />
                            <a href="">Check Applications</a><br />
                            <a href="">Jobseeker FAQs</a><br />
                            <a href="">Browse Jobs</a><br />
                            <a href="">Browse by Industry</a><br />
                        </td>
                        <td className="td-footer">
                            EMPLOYER<br /><br />
                            <a href="">Create Account</a><br />
                            <a href="">Post Jobs</a><br />
                            <a href="">Products & Prices</a><br />
                            <a href="">Customer Support</a><br />
                            <a href="">Contact Sales</a><br /><br />
                        </td>
                        <td className="td-footer">
                            CONNECT WITH US<br /><br />
                            <text className="icon"><FaFacebookF /></text><a className="a-socmed" href="">Facebook</a><br />
                            <text className="icon"><FaTwitter /></text><a className="a-socmed" href="">Twitter</a><br />
                            <text className="icon"><FaLinkedinIn /></text><a className="a-socmed" href="">LinkedIn</a><br />
                            <text className="icon"><FaInstagram /></text><a className="a-socmed" href="">Instagram</a><br />
                            <a href="">Call us at +603 2716 5199</a><br />
                            <a href="">Send us a message</a><br />
                        </td>
                    </tr>
                </table>
            </div>
        )
    }

    return (
        <div>
            <div className="body-6">
                <div className="body-7">
                    jobstore.com<text className="footer-registered">&reg;</text>
                    <br /><br />
                    {tableFooter()}
                </div>
                <div className="body-8">
                    <text className="footer-copyright">&copy;</text> 2023 Jobstore
                    <text className="footer-bullet">&bull;</text>Terms of Service
                    <text className="footer-bullet">&bull;</text>Privacy Policy
                    <text className="footer-bullet">&bull;</text>All Rights Reserved
                    <br /><br /><hr />
                    <p className="text-footer">
                        <br />Jobstore is the biggest job distribution and job search platform with a broad presence in Malaysia, Australia, Hong Kong, Singapore, the Philippines, and Indonesia.<br /><br />
                        It is currently one of the fastest growing job portals in the Asia Pacific region — recognized with multiple awards such as Top 100 Private Technology Company, Top 10 Consumer Cloud Application, etc. Having more than 20,000 top firms and a growing number of career opportunities on board, Jobstore connects the right people with the right jobs, serving tens of thousands of employers of all sizes and industries.<br /><br />
                        Furthermore, our social networking search technology is capable of tracking users' online behavior, thus ensuring that jobseekers are presented with the most recent and relevant jobs from their search.<br /><br />
                        Jobstore has also streamlined the hiring process radically, making it much more organized and efficient. With just one submission, employers and recruiters are able to advertise jobs on multiple job sites, classified ads and social network sites. We also integrate a state-of-the-art application tracking system that will help your recruitment team work more efficiently.<br /><br />
                        Trusted by 8 million users, we refuse to settle and will continue to explore new frontiers across all emerging online channels. We strive to gain new insights and understanding of the job market so that we will help not only employers to fill their vacancies, but also help jobseekers to land the jobs that match their true potential.<br /><br />
                        We believe everyone can, and should love his or her job. And Jobstore is where you'll find your calling.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default Footer;