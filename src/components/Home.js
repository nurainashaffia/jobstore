import TotalJobs from "./TotalJobs";
import FeaturedEmployers from "./FeaturedEmployers";
import TrendingJobs from "./TrendingJobs";
import Search from "./Search";
import Footer from "./Footer";

function Home() {
    return (
        <div>
            <TotalJobs />
            <FeaturedEmployers />
            <TrendingJobs />
            <Search />
            <Footer />
        </div>
    )
}

export default Home;